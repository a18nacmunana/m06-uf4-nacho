package cat.inspedralbes.m06;

import java.util.List;

import cat.inspedralbes.m6.uf4.clients.IClient;

public class SqlRepo implements IClientRepo{
	private TelefonDAO telfDAO = new TelefonDAO();
	private ClientDAO clientDAO = new ClientDAO();
	
	@Override
	public IClient read(String nif) {
		IClient client = clientDAO.read(nif);
		List<String> telefons = telfDAO.read(client.getNIF());
		for (String telf : telefons)
			client.addTelefon(telf);
		return client;
	}

	@Override
	public void create(IClient client) {
		clientDAO.create(client);
		
		for(String telf : client.getTelefons()) {
			telfDAO.create(telf, client.getNIF());
		}
	}

	@Override
	public void update(IClient client) {
		clientDAO.update(client);
		telfDAO.update(client.getTelefons(), client.getNIF());
	}

	@Override
	public void delete(IClient client) {
		clientDAO.delete(client);
		telfDAO.delete(client.getNIF());
	}

}
