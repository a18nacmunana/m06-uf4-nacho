package cat.inspedralbes.m06;

import java.util.List;


public interface ITelefonDAO {
	void create(String telf, String client);
	List<String> read(String id);
	void update(List<String> telfs, String nifClient);
	void delete(String nif);
}
