package cat.inspedralbes.m06;

import cat.inspedralbes.m6.uf4.clients.IClient;

public class Principal {

	private static boolean comparaClients(IClient a, IClient b) {

		if (!a.getNIF().equals(b.getNIF())) {
			return false;
		}
		if (!a.getNom().equals(b.getNom())) {
			return false;
		}
		if (a.getTelefons().size() != b.getTelefons().size()) {
			return false;
		}
		for (int i = 0; i < a.getTelefons().size(); i++) {
			String telefA = a.getTelefons().get(i);
			String telefB = b.getTelefons().get(i);
			if (!telefA.equals(telefB)) {
				return false;
			}
		}
		return true;
	}

///////////////////////////////////////////////////////
// TODO: Posa aqui el nom de les teves classes
///////////////////////////////////////////////////////
	static String nomClasseClientsRepositoriMySQL = "cat.inspedralbes.m06.SqlRepo";
	static String nomClasseClientsRepositoriMock = "cat.inspedralbes.m06.MockRepo";

	static String nomClasseClient = "cat.inspedralbes.m06.Client";
///////////////////////////////////////////////////////

	public static void main(String[] args) {
	
		// TODO: Crea un client a partir del nom que esta en l'string, no fent un new
		IClient clientA = new Client();
		clientA.setNIF("11111111A");
		clientA.setNom("Andreu");
		clientA.addTelefon("911111111");

		// TODO: Crea un client a partir del nom que esta en l'string, no fent un new
		IClient clientB = new Client();
		clientB.setNIF("22222222B");
		clientB.setNom("Bea");
		clientB.addTelefon("921111111");
		clientB.addTelefon("922111111");

// TODO: Crea un IClientsRepo a partir del nom que esta en l'string, no fent un new 
// Prova-ho primer amb un DAO i despres amb un altre
		IClientRepo clientRepo = new SqlRepo();

// Utilitzacio normal
		clientRepo.create(clientA);
		clientRepo.create(clientB);

// IMPRESSIÓ
		System.out.println("Clients guardats");
		System.out.println("--------------------------------");
		System.out.println(clientA.getNom());
		System.out.println(clientB.getNom());

		IClient llegitA = clientRepo.read("11111111A");
		IClient llegitB = clientRepo.read("22222222B");
		

// IMPRESSIÓ
// S'ha de veure el mateix que abans
		System.out.println("Clients llegits");
		System.out.println("--------------------------------");
		System.out.println(llegitA.getNom());
		System.out.println(llegitB.getNom());

		if (comparaClients(llegitA, clientA) && comparaClients(llegitB, clientB)) {
			System.out.println("Els clients llegits coincideixen amb els guardats");
		} else {
			System.out.println("Els clients llegits no coincideixen amb els guardats");
		}
		
		clientA.setNom("Pepe");
		clientA.removeTelefon("911111111");
		clientA.addTelefon("111111111");
		clientRepo.update(clientA);
		IClient updatedClient = clientRepo.read(clientA.getNIF());
		System.out.println("El nom actualitzat es: "+ updatedClient.getNom());
		System.out.print("El seu telèfon ara és: ");
		for(String telf : updatedClient.getTelefons())
			System.out.print(telf );
		
		clientRepo.delete(clientA);
		
	}
}
