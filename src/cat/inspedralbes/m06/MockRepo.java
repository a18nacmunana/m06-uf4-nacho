package cat.inspedralbes.m06;


import cat.inspedralbes.m6.uf4.clients.IClient;

public class MockRepo implements IClientRepo{
	private MockClientDAO clientDAO = new MockClientDAO();
	
	@Override
	public IClient read(String nif) {
		return clientDAO.read(nif);
	}

	@Override
	public void create(IClient client) {
		clientDAO.create(client);
	}

	@Override
	public void update(IClient client) {
		clientDAO.update(client);
	}

	@Override
	public void delete(IClient client) {
		clientDAO.delete(client);
	}
}
