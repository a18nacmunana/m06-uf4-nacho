package cat.inspedralbes.m06;

import java.util.ArrayList;
import java.util.List;

import cat.inspedralbes.m6.uf4.clients.IClient;

public class MockClientDAO implements IClientDAO<IClient>{
	
	List<IClient> clients = new ArrayList<>();

	
	@Override
	public void create(IClient c) {
		clients.add(c);
	}

	@Override
	public IClient read(String nif) {
		for(IClient client : clients) {
			if(client.getNIF().equals(nif))
				return client;
		}
		System.out.println("retorno null");
		return null;
	}

	@Override
	public void update(IClient c) {
		for(int i=0; i<clients.size();i++) {
			if(clients.get(i).getNIF().equals(c.getNIF()))
				clients.set(i, c);
		}
	}

	@Override
	public void delete(IClient c) {
		for(int i=0; i<clients.size();i++) {
			if(clients.get(i).getNIF().equals(c.getNIF()))
				clients.remove(i);
		}
	}

}
