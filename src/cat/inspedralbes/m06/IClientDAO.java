package cat.inspedralbes.m06;

public interface IClientDAO <Object>{
	void create(Object object);
	Object read(String id);
	void update(Object object);
	void delete(Object object);
}
