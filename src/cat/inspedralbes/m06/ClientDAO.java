package cat.inspedralbes.m06;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import cat.inspedralbes.m6.uf4.clients.IClient;

public class ClientDAO implements IClientDAO <IClient>{
	Connection con;

	ClientDAO(){
		try {
			con = DriverManager.getConnection(ConnectionData.URL, ConnectionData.USERNAME, ConnectionData.PASSWORD);
		} catch (SQLException e1) {
			System.out.println("ERROR a la conexió");
			e1.printStackTrace();
		}
	}
	@Override
	public void create(IClient client) {
		try {
			Statement st = con.createStatement();
			st.executeUpdate("INSERT INTO Client (nom, nif) VALUES ('"+client.getNom()+"','"+client.getNIF()+"');");
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public IClient read(String nif) {
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM Client WHERE nif="+"\""+nif+"\"");
			rs.next();
			String nifRead = rs.getString("nif");
			String nomRead = rs.getString("nom");
			Client client = new Client();
			client.setNIF(nifRead);
			client.setNom(nomRead);
			st.close();
			return client;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void update(IClient client) {
		try {
			Statement st = con.createStatement();
			st.executeUpdate("UPDATE Client SET nom=\""+client.getNom()+"\""+" WHERE nif="+"\""+client.getNIF()+"\"");
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(IClient client) {
		try {
			Statement st = con.createStatement();
			st.executeUpdate("DELETE FROM Client WHERE nif="+"\""+client.getNIF()+"\"");
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
