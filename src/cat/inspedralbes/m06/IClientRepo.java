package cat.inspedralbes.m06;

import cat.inspedralbes.m6.uf4.clients.IClient;

public interface IClientRepo{
	IClient read(String nif);
    void create(IClient client);
    void update(IClient client);
    void delete(IClient client);
}
