package cat.inspedralbes.m06;

import java.util.ArrayList;
import java.util.List;

import cat.inspedralbes.m6.uf4.clients.IClient;

public class Client implements IClient{
	
	String nom;
	String nif;
	List<String> telf = new ArrayList<>();


	@Override
	public void addTelefon(String telefon) {
		this.telf.add(telefon);
	}

	@Override
	public void removeTelefon(String telefon) {
		this.telf.remove(telefon);
	}

	@Override
	public List<String> getTelefons() {
		return this.telf;
	}

	@Override
	public void setNom(String nom) {
		this.nom=nom;
	}

	@Override
	public String getNom() {
		return this.nom;
	}

	@Override
	public void setNIF(String nif) {
		this.nif=nif;
	}

	@Override
	public String getNIF() {
		return this.nif;
	}

}
