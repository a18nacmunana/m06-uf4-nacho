package cat.inspedralbes.m06;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import cat.inspedralbes.m6.uf4.clients.IClient;


public class TelefonDAO implements ITelefonDAO {
	Connection con;
	
	TelefonDAO(){
		try {
			con = DriverManager.getConnection(ConnectionData.URL, ConnectionData.USERNAME, ConnectionData.PASSWORD);
		} catch (SQLException e1) {
			System.out.println("ERROR a la conexió");
			e1.printStackTrace();
		}
	}

	@Override
	public void create(String telf, String client) {
		try {
			Statement st = con.createStatement();
			st.executeUpdate("INSERT INTO Telefon (numero, nif_client) VALUES ('"+telf+"','"+client+"');");
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<String> read(String nif) {
		List<String> telefons= new ArrayList<>();
		try {
			Statement st = con.createStatement();
			ResultSet rs =st.executeQuery("SELECT * FROM Telefon WHERE nif_client=\""+nif+"\"");
			while(rs.next()) {
				telefons.add(rs.getString("numero"));
			}
			st.close();
			return telefons;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void update(List<String> telfs, String nifClient) {
		try {
			Statement st = con.createStatement();
			st.executeUpdate("DELETE FROM Telefon WHERE nif_client="+"\""+nifClient+"\"");
			
			for(String telf : telfs) {
				st.executeUpdate("INSERT INTO Telefon (numero, nif_client) VALUES ('"+telf+"','"+nifClient+"');");
			}
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(String nif) {
		try {
			Statement st = con.createStatement();
			st.executeUpdate("DELETE FROM Telefon WHERE nif_client="+"\""+nif+"\"");
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

}
